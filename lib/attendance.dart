
import 'dart:async';
import 'dart:convert';
import 'globals.dart' as globals;
import 'package:flutter/material.dart';
import 'package:flutter_app/attendance_detail.dart';
import 'package:flutter_app/punchout.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:toast/toast.dart';
import 'package:flutter_app/punchin.dart';
import 'package:flutter_app/main.dart';
import 'package:date_format/date_format.dart';

class Attendance extends StatefulWidget {
  @override
  _AttendanceState createState() => _AttendanceState();
}

class _AttendanceState extends State<Attendance> {

  List data;
  bool _isLoading = false;

  Widget punchout(id) {
    return IconSlideAction(
      caption: "Punch Out",
      color: Colors.amber.shade300,
      icon: Icons.location_off,
      closeOnTap: true,
      onTap: () {
        var route = new MaterialPageRoute(
            builder: (BuildContext context) => new Punchout(value: id),
        );
        Navigator.of(context).pushAndRemoveUntil(route, (Route<dynamic> route) => false);
      },
    );
  }

  Widget attendancedetail(id) {
    return IconSlideAction(
      caption: 'View',
      color: Colors.green,
      icon: Icons.assignment,
      closeOnTap: true,
      onTap: () {
        var route = new MaterialPageRoute(
          builder: (BuildContext context) => new AttendanceDetail(value: id),
        );
        Navigator.of(context).push(route);
      },
    );
  }

  void postDelete(id) async {

    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    var token = sharedPreferences.getString("token");
    Map data = {
      'id' : id,
    };

    var response = await http.post(globals.apihost + "attendance/delete", body: data,
        headers: {"Accept": "application/json", "Authorization": "Bearer " + token});
    if(response.statusCode == 200) {
      Navigator.of(context).pop();
      this.getData();
      Toast.show('Record successfully deleted', context, duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
    }
  }

  Future<String> getData() async {
    setState(() {
      _isLoading = true;
    });
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    var token = sharedPreferences.getString("token");
    var response = await http.get(Uri.encodeFull(globals.apihost + "attendance/list-current-month"),
        headers: {"Accept": "application/json", "Authorization": "Bearer " + token});

    if(response.statusCode == 200) {
      setState(() {
        _isLoading = false;
      });
    }
    this.setState(() {
      data = json.decode(response.body);
    });

    return "Success!";
  }

  @override
  void initState() {
    super.initState();
    this.getData();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("Attendance"),
      ),
      body: _isLoading ? 
      Center(child: CircularProgressIndicator()) :
      Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors: [Colors.white, Colors.blue]
          ),
        ),
        child: ListView.builder(
          itemCount: data == null ? 0 : data.length,
          itemBuilder: (context, index) {
            return Slidable(key:ValueKey(index),
              child: Card(
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      Expanded(
                        flex: 5,
                        child: Column(
                          children: <Widget>[
                            new Padding(
                              padding: const EdgeInsets.all(0.0),
                              child: Column(
                                children: <Widget>[
                                  new ListTile(
  //                                    contentPadding: EdgeInsets.symmetric(horizontal: 0.0),
                                    leading: Column(
                                      children: <Widget>[
                                        data[index]["punchinsite"] == "0"? Icon(Icons.location_on, color: Colors.blue):Icon(Icons.location_on, color: Colors.green), // icon
                                        data[index]["punchinsite"] == "0"? Text("IN"):Text("SITE"),
                                      ],
                                    ),
                                    title: new Text(data[index]["location"],overflow: TextOverflow.ellipsis,),
                                    subtitle: new Text("${formatDate(DateTime.parse("${data[index]["in_dt"]}"),[dd, '/', mm, '/', yyyy, ' ', hh, ':', nn, ':', ' ', am])}"),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      Expanded(
                        flex: 5,
                        child: Column(
                          children: <Widget>[
                            new Padding(
                              padding: const EdgeInsets.all(3.0),
                              child: Column(
                                children: <Widget>[
                                  new ListTile(
  //                                  contentPadding: EdgeInsets.symmetric(horizontal: 0.0),
                                    leading: data[index]["punchinsite"] == "0"? Column(
                                      children: <Widget>[
                                        Icon(Icons.location_off, color: Colors.amber), // icon
                                        Text("OUT"), // text
                                      ],
                                    ):null,
                                    title: Text(data[index]["locationout"] != null? data[index]["locationout"]:"",overflow: TextOverflow.ellipsis),
                                    subtitle: Text(data[index]["out_dt"] != null? formatDate(DateTime.parse("${data[index]["out_dt"]}"),[dd, '/', mm, '/', yyyy, ' ', hh, ':', nn, ':', ' ', am]):""),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  )
              ),
              actionPane: SlidableDrawerActionPane(),
              actions: <Widget>[
                attendancedetail(data[index]["id"]),
              ],
              secondaryActions: <Widget>[
                IconSlideAction(
                  caption: "Delete",
                  color: Colors.red.shade300,
                  icon: Icons.delete,
                  closeOnTap: false,
                  onTap: () {
                    showDialog(
                        context: context,
                        builder: (context) {
                          return AlertDialog(
                            title: Text('Confirmation'),
                            content: const Text(
                                'Are you sure to delete this record?'),
                            actions: <Widget>[
                              FlatButton(
                                child: const Text('CANCEL'),
                                onPressed: () {
                                  Navigator.of(context).pop();
                                },
                              ),
                              FlatButton(
                                child: const Text('CONFIRM'),
                                onPressed: () {
                                  postDelete(data[index]["id"]);
                                },
                              )
                            ],
                          );
                        }
                    );
                  },
                ),
                if (data[index]["punchinsite"] == "0" && data[index]["locationout"] == null)
                  punchout(data[index]["id"]),
              ],
            );
          },
        ),
      ),
      drawer: Drawer(
        child: Column(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(top: 30),
            ),
            ListTile(
              leading: Icon(Icons.dashboard),
              title: Text('Dashboard'),
              onTap: () {
                Navigator.pushAndRemoveUntil(context, new MaterialPageRoute(builder: (context) => new MainPage()), (Route<dynamic> route) => false);
              },
            ),
            ListTile(
              leading: Icon(Icons.format_list_bulleted),
              title: Text('Attendance'),
              onTap: () {
                Navigator.push(context, new MaterialPageRoute(builder: (context) => new Attendance()));
              },
            ),
            ListTile(
              leading: Icon(Icons.location_on),
              title: Text('Punch In'),
              onTap: () {
                Navigator.push(context, new MaterialPageRoute(builder: (context) => new Punchin()));
              },
            )
          ],
        ),
      ),
    );
  }
}
