import 'package:flutter/material.dart';
import 'package:flutter_app/punchin.dart';
import 'package:flutter_app/attendance.dart';
import 'login_page.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() {
  runApp(new MyApp());
}

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
      return MaterialApp(
        title: "OSTHR",
        debugShowCheckedModeBanner: false,
        home: LoginPage(),
        theme: ThemeData(
            accentColor: Colors.white70
        ),
      );
  }
}

class MainPage extends StatefulWidget {
  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {

  SharedPreferences sharedPreferences;

  @override
  void initState() {
    super.initState();
    checkLoginStatus();
  }

  checkLoginStatus() async {
    sharedPreferences = await SharedPreferences.getInstance();
    if(sharedPreferences.getString("token") == null) {
      Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (BuildContext context) => LoginPage()), (Route<dynamic> route) => false);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("OSTHR", style: TextStyle(color: Colors.white)),
        actions: <Widget>[
          FlatButton(
            onPressed: () {
              sharedPreferences.remove("token");
              Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (BuildContext context) => LoginPage()), (Route<dynamic> route) => false);
            },
            child: Text("Log Out", style: TextStyle(color: Colors.white)),
          ),
        ],
      ),
      body: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors: [Colors.white, Colors.blue]
          ),
        ),
        child: Center(child: Text("Welcome to OSTHR Mobile")),
      ),
      drawer: Drawer(
        child: Column(
          children: <Widget>[
            Padding(
                padding: EdgeInsets.only(top: 20),
            ),
            ListTile(
                leading: Icon(Icons.location_on),
                title: Text('Attendance'),
                onTap: () {
                  Navigator.push(context, new MaterialPageRoute(builder: (context) => new Attendance()));
                },
            ),
            ListTile(
              leading: Icon(Icons.location_on),
              title: Text('Punch In'),
              onTap: () {
                Navigator.push(context, new MaterialPageRoute(builder: (context) => new Punchin()));
              },
            )
          ],
        ),
      ),
    );
  }
}