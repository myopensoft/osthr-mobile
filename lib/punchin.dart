import 'globals.dart' as globals;
import 'package:flutter/material.dart';
import 'package:flutter_app/attendance.dart';
import 'package:http/http.dart' as http;
import 'package:geolocator/geolocator.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Punchin extends StatefulWidget {
  @override
  _PunchinState createState() => _PunchinState();
}

class _PunchinState extends State<Punchin> {
  final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;

  Position _currentPosition;
  String _currentAddress;
  String _remarks;
  bool _isLoading = false;
  final formKey = new GlobalKey<FormState>();

  bool validateAndSave() {
    final form = formKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  @override
  void initState() {
    super.initState();
    _getCurrentLocation();
  }

  void postPunchIn() async {
    if (validateAndSave()) {
      SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
      var token = sharedPreferences.getString("token");
      Map data = {
        'long' : _currentPosition.longitude.toString(),
        'lat' : _currentPosition.latitude.toString(),
        'location' : _currentAddress,
        'remarks' : _remarks,
      };
      var response = await http.post(globals.apihost + "attendance/punchin",
                      body: data,
                      headers: {"Accept": "application/json", "Authorization": "Bearer " + token});
      if(response.statusCode == 200) {
        setState(() {
          _isLoading = false;
        });
        Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (BuildContext context) => Attendance()), (Route<dynamic> route) => false);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: new Text("Punch In"),
      ),
      body: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors: [Colors.white, Colors.blue]
          ),
        ),
        child: _isLoading ? Center(child: CircularProgressIndicator(backgroundColor: Colors.blue)) : Form(
          key: formKey,
          child: Center(
            child: ListView(
              children: <Widget>[
                Card(
                  child: ListTile(
                    leading: Column(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Icon(Icons.location_on, color: Colors.green)
                      ],
                    ),
                    title: Text(_currentAddress.toString()),
                  ),
                ),
                Card(
                  child: ListTile(
                    leading: Column(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Icon(Icons.receipt, color: Colors.green)
                      ],
                    ),
                    title: TextFormField(
                      decoration: InputDecoration(labelText: "Remarks"),
                      onSaved: (value) => _remarks = value,
                    ),
                  ),
                ),
                Padding(
                    padding: EdgeInsets.symmetric(vertical: 1.0),
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        RaisedButton(
                          child: Row(
                            children: <Widget>[
                              Icon(Icons.my_location, size: 20),
                              Text(" Reload Location"),
                            ],
                          ),
                          onPressed: () {
                            _getCurrentLocation();
                          },
                          color: Colors.lightGreen,
                        ),
                        RaisedButton(
                          child: Row(
                            children: <Widget>[
                              Icon(Icons.location_on, size: 20),
                              Text(" Punch In"),
                            ],
                          ),
                          onPressed: () {
                            postPunchIn();
                          },
                          color: Colors.lightBlueAccent,
                        ),
                      ],
                    ),
                )
              ],
            ),
          ),
        )
      ),
    );
  }

  _getCurrentLocation() {
    setState(() {
      _isLoading = true;
    });
    geolocator
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.best)
        .then((Position position) {
      setState(() {
        _currentPosition = position;
      });

      _getAddressFromLatLng();
    }).catchError((e) {
      print(e);
    });
  }

  _getAddressFromLatLng() async {
    try {
      List<Placemark> p = await geolocator.placemarkFromCoordinates(
          _currentPosition.latitude, _currentPosition.longitude);

      Placemark place = p[0];
      if(place != null) {
        setState(() {
          _isLoading = false;
        });
      }
      setState(() {
        _currentAddress = "${place.name}, ${place.thoroughfare}, ${place.subLocality}, ${place.locality}, ${place.postalCode}, ${place.administrativeArea}, ${place.country}";
      });
    } catch (e) {
      print(e);
    }
  }

}
