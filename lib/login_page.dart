import 'dart:convert';
import 'globals.dart' as globals;
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_app/main.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:toast/toast.dart';

class LoginPage extends StatefulWidget {

  @override
  State<StatefulWidget> createState() => new _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {

  bool _isLoading = false;
  final formKey = new GlobalKey<FormState>();

  String _username;
  String _password;

  bool validateAndSave() {
    final form = formKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  void _showAlert(BuildContext context,status,message) {
      Toast.show('$status : $message', context, duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
    }

  void validateAndSubmit() async {
    if (validateAndSave()) {
      SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
      Map data = {
        'username': _username,
        'password': _password
      };
      var response = await http.post(globals.apihost + "auth/login", body: data);
      if(response.statusCode == 200) {
        setState(() {
          _isLoading = false;
        });
        var jsonResponse = json.decode(response.body);
        print(jsonResponse);
        if (jsonResponse != null) {
          sharedPreferences.setString("token", jsonResponse['data']['token']);
          print(jsonResponse['data']['token']);
          Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (BuildContext context) => MainPage()), (Route<dynamic> route) => false);
        }
      }else{
        setState(() {
          _isLoading = false;
        });
        var jsonResponse = json.decode(response.body);
        print(jsonResponse);
        _showAlert(context,jsonResponse['status'],jsonResponse['message']);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text('OSTHR'),
      ),
      body: new Container(
        padding: EdgeInsets.all(16.0),
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors: [Colors.white, Colors.blue]
          ),
        ),
        child: _isLoading ? Center(child: CircularProgressIndicator()) : new Form(
          key: formKey,
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Image.asset('assets/icon/OST_logo_Circle.png', scale: 2.0),
                ],
              ),
              TextFormField(
                decoration: InputDecoration(labelText: 'Username'),
                validator: (value) => value.isEmpty ? 'Username can\'t be empty' : null,
                onSaved: (value) => _username = value,
              ),
              TextFormField(
                decoration: InputDecoration(labelText: 'Password'),
                obscureText: true,
                  validator: (value) => value.isEmpty ? 'Email can\'t be empty' : null,
                  onSaved: (value) => _password = value,
              ),
              ConstrainedBox(
                constraints: BoxConstraints(
                  minHeight: 80,
                ),
                child: Padding(
                  padding: EdgeInsets.all(16.0),
                  child: RaisedButton(
                    child: new Text('Login'),
                    color: Colors.lightGreen,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(18.0),
                    ),
                    onPressed: () {
                      setState(() {
                        _isLoading = true;
                      });
                      validateAndSubmit();
                    }
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}