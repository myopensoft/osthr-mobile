import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:date_format/date_format.dart';
import 'globals.dart' as globals;

class AttendanceDetail extends StatefulWidget {
  final String value;
  AttendanceDetail({Key key, this.value}) : super (key: key);

  @override
  _AttendanceDetailState createState() => _AttendanceDetailState();
}

class _AttendanceDetailState extends State<AttendanceDetail> {
  String location;
  String indt;
  String locationout;
  String outdt;
  String remarks;
  bool _isLoading = false;

  @override
  void initState() {
    super.initState();
    _getAttendanceDetail();
  }

  _getAttendanceDetail() async {
    setState(() {
      _isLoading = true;
    });
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    var token = sharedPreferences.getString("token");
    Map data = {
      'id' : widget.value,
    };

    var response = await http.post(globals.apihost + "attendance/list-detail", body: data,
        headers: {"Accept": "application/json", "Authorization": "Bearer " + token});
    if(response.statusCode == 200) {
      setState(() {
        _isLoading = false;
      });
      data = json.decode(response.body);
      setState(() {
        location = data["location"];
        indt = data["in_dt"];
        locationout = data["locationout"];
        outdt = data["out_dt"];
        remarks = data["remarks"];
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Attendance Detail"),
      ),
      body: _isLoading ? 
      Center(child: CircularProgressIndicator()) : 
      Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors: [Colors.white, Colors.blue]
          ),
        ),
        child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.all(16.0),
                  child: Column(
                    children: <Widget>[
                      Icon(Icons.location_on, color: Colors.green),
                      Text("Punch In"),
                    ],
                  ),
                ),
                Flexible(
                    child: Padding(
                      padding: EdgeInsets.all(16.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text("$location"),
                          Divider(),
                          Text("${formatDate(DateTime.parse("$indt"),[dd, '/', mm, '/', yyyy, ' ', hh, ':', nn, ' ', am])}", textAlign: TextAlign.left),
                        ],
                      ),
                    )
                ),
              ],
            ),
            Row(
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.all(10.0),
                  child: Column(
                    children: <Widget>[
                      Icon(Icons.location_off, color: Colors.amber),
                      Text("Punch Out"),
                    ],
                  ),
                ),
                Flexible(
                    child: Padding(
                      padding: EdgeInsets.all(16.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(locationout != null? "$locationout":""),
                          Divider(),
                          Text(outdt != null? "${formatDate(DateTime.parse("$outdt"),[dd, '/', mm, '/', yyyy, ' ', hh, ':', nn, ' ', am])}":""),
                        ],
                      ),
                    )
                ),
              ],
            ),
            Row(
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.all(16.0),
                  child: Column(
                    children: <Widget>[
                      Icon(Icons.receipt, color: Colors.grey),
                      Text("Remarks"),
                    ],
                  ),
                ),
                Flexible(
                    child: Padding(
                      padding: EdgeInsets.all(16.0),
                      child: Column(
                        children: <Widget>[
                          Text(remarks != null? "$remarks":""),
                        ],
                      ),
                    )
                ),
              ],
            ),
          ],
        )
      ),
    );
  }
}
