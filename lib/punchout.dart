import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_app/attendance.dart';
import 'package:flutter_app/main.dart';
import 'package:flutter_app/punchin.dart';
import 'globals.dart' as globals;

class Punchout extends StatefulWidget {
  final String value;

  Punchout({Key key, this.value}) : super (key: key);

  @override
  _PunchoutState createState() => _PunchoutState();
}

class _PunchoutState extends State<Punchout> {
  final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;

  Position _currentPosition;
  String _currentAddress;
  bool _isLoading = false;

  @override
  void initState() {
    super.initState();
    _getCurrentLocation();
  }

  void postPunchOut() async {

    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    var token = sharedPreferences.getString("token");
    Map data = {
      'id' : widget.value,
      'long' : _currentPosition.longitude.toString(),
      'lat' : _currentPosition.latitude.toString(),
      'location' : _currentAddress,
    };
    var response = await http.post(globals.apihost + "attendance/punchout",
                    body: data,
                    headers: {"Accept": "application/json", "Authorization": "Bearer " + token});
    if(response.statusCode == 200) {
      setState(() {
        _isLoading = false;
      });
      Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (BuildContext context) => Attendance()), (Route<dynamic> route) => false);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        title: new Text("Punch Out"),
      ),
      body: _isLoading ? Center(child: CircularProgressIndicator()) : 
      Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors: [Colors.white, Colors.blue]
          ),
        ),
        child: Center(
          child: ListView(
            children: <Widget>[
              new Card(
                child: new ListTile(
                  leading: Column(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      new Icon(Icons.location_on, color: Colors.green)
                    ],
                  ),
                  title: Text(_currentAddress.toString()),
                ),
              ),
              new Padding(
                  padding: EdgeInsets.symmetric(vertical: 1.0),
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      RaisedButton(
                        child: Row(
                          children: <Widget>[
                            Icon(Icons.my_location, size: 20),
                            Text(" Reload Location"),
                          ],
                        ),
                        onPressed: () {
                          _getCurrentLocation();
                        },
                        color: Colors.lightGreen,
                      ),
                      RaisedButton(
                        child: Row(
                          children: <Widget>[
                            Icon(Icons.location_off, size: 20),
                            Text(" Punch Out"),
                          ],
                        ),
                        onPressed: () {
                          postPunchOut();
                        },
                        color: Colors.lightBlueAccent,
                      ),
                    ],
                  ),
              )
            ],
          ),
        ),
      ),
      drawer: Drawer(
        child: Column(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(top: 30),
            ),
            ListTile(
              leading: Icon(Icons.dashboard),
              title: Text('Dashboard'),
              onTap: () {
                Navigator.pushAndRemoveUntil(context, new MaterialPageRoute(builder: (context) => new MainPage()), (Route<dynamic> route) => false);
              },
            ),
            ListTile(
              leading: Icon(Icons.format_list_bulleted),
              title: Text('Attendance'),
              onTap: () {
                Navigator.push(context, new MaterialPageRoute(builder: (context) => new Attendance()));
              },
            ),
            ListTile(
              leading: Icon(Icons.location_on),
              title: Text('Punch In'),
              onTap: () {
                Navigator.push(context, new MaterialPageRoute(builder: (context) => new Punchin()));
              },
            )
          ],
        ),
      ),
    );
  }

  _getCurrentLocation() {
    setState(() {
      _isLoading = true;
    });
    geolocator
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.best)
        .then((Position position) {
      setState(() {
        _currentPosition = position;
      });

      _getAddressFromLatLng();
    }).catchError((e) {
      print(e);
    });
  }

  _getAddressFromLatLng() async {
    try {
      List<Placemark> p = await geolocator.placemarkFromCoordinates(
          _currentPosition.latitude, _currentPosition.longitude);

      Placemark place = p[0];
      if(place != null) {
        setState(() {
          _isLoading = false;
        });
      }
      setState(() {
        _currentAddress =
        "${place.name}, ${place.thoroughfare}, ${place.subLocality}, ${place.locality}, ${place.postalCode}, ${place.administrativeArea}, ${place.country}";
      });
    } catch (e) {
      print(e);
    }
  }
}
